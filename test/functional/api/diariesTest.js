const chai = require("chai");
const expect = chai.expect;
const request = require("supertest");
const {
  MongoClient
} = require("mongodb");
const dotenv = require("dotenv");
dotenv.config();

const _ = require("lodash");

let server, db, client, collection, validID, typeMov, genre;

describe("Diaries", () => {
  before(async () => {
    try {
      client = await MongoClient.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      });
      db = client.db(process.env.MONGO_DB);
      collection = db.collection("diaries");
      server = require("../../../bin/www");
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await collection.deleteMany({});
      await collection.insertOne({
        type: "Series",
        genre: "Fiction",
        favorite: "Game Of Thrones",
        stars: 8,
        comment: "Amazing Show!",
        upvotes: 2
      });
      await collection.insertOne({
        type: "Movie",
        genre: "Action",
        favorite: "Bad Boys 3",
        stars: 6,
        comment: "One of will Smith's best movies!",
        upvotes: 2
      });
      const diary = await collection.findOne({
        favorite: "Game of Thrones"
      })
      validID = diary._id
      typeMov = diary.type
      genre = diary.genre

    } catch (error) {
      console.log(error);
    }
  });

  // describe("GET /diaries", () => {
  //   it("should GET all the diarys entry", done => {
  //     request(server)
  //       .get("/diaries")
  //       .set("Accept", "application/json")
  //       .expect("Content-Type", /json/)
  //       .expect(200)
  //       .end((err, res) => {
  //         try {
  //           expect(res.body).to.be.a("array")
  //          expect(res.body.length).to.equal(0);
  //           let result = _.map(res.body, diary => {
  //             return {
  //               type: diary.type,
  //               genre: diary.genre,
  //             }
  //           })
  //           expect(result).to.deep.include({
  //             type: "Series",
  //             genre: "Fiction",
  //           })

  //           expect(result).to.deep.include({
  //             type: "Movie",
  //             genre: "Action"
  //           })
  //           done()
  //         } catch (e) {
  //           done(e)
  //         }
  //       })
  //   })






  describe("GET /diaries/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching diary", done => {
        request(server)
          .get(`/diaries/${validID}`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) => {
            // expect(res.body[0]).to.have.property("type", "Series")
            // expect(res.body[0]).to.have.property("favorite", "Game Of Thrones")
            done(err)
          })
      })
    })
    describe("when the id is invalid", () => {
      it("should return the NOT found message", done => {
        request(server)
          .get("/diaries/9999")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) => {
            expect(res.body.message).equals("Diary Entry NOT Found!!!")
            done(err)
          })
      })
    })
  })



  describe("GET /diaries/type/:type", () => {
    describe("when the id is valid", () => {
      it("should return the matching diary", done => {
        request(server)
          .get(`/diaries/type/${typeMov}`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) => {
            // expect(res.body[0]).to.have.property("type", "Movie")
            // expect(res.body[0]).to.have.property("genre", "Action")
            done(err)
          })
      })
    })
    describe("when the type is invalid", () => {
      it("should return the NOT found message", done => {
        request(server)
          .get("/diaries/Series")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) => {
            expect(res.body.message).equals("Diary Entry NOT Found!!!")
            done(err)
          })
      })
    })
  })

  describe("GET /diaries/genre/:genre", () => {
    describe("when the genre is valid", () => {
      it("should return the matching diary", done => {
        request(server)
          .get(`/diaries/genre/${genre}`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) => {
            // expect(res.body[0]).to.have.property("genre", "Action")
            // expect(res.body[0]).to.have.property("favorite", "Bad Boys 3")
            done(err)
          })
      })
    })
    describe("when the type is invalid", () => {
      it("should return the NOT found message", done => {
        request(server)
          .get("/diaries/Comedy")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) => {
            expect(res.body.message).equals("Diary Entry NOT Found!!!")
            done(err)
          })
      })
    })
  })


})

describe("POST /diaries", () => {
  it("should return confirmation message and update datastore", () => {
    const diary = {
      type: "Movie",
      genre: "Fiction",
      favorite: "The Witcher",
      stars: 7,
      comments: "Amazing Movie",
      upvotes: 0
    }
    return request(server)
      .post("/diaries")
      .send(diary)
      .expect(200)
      .then(res => {
        expect(res.body.message).equals("Diary Successfully Added!")
        validID = res.body.data._id
      })
  })
  after(() => {
    return request(server)
      .get(`/diaries/${validID}`)
      .expect(200)
      .then(res => {
        expect(res.body[0]).to.have.property("type", "Movie")
        expect(res.body[0]).to.have.property("favorite", "The Witcher")

      })
  })
})



describe("PUT /diaries/:id/vote", () => {
  describe("when the id is valid", () => {
    it("should return a message and the diary upvoted by 1", () => {
      return request(server)
        .put(`/diaries/${validID}/vote`)
        .expect(200)
        .then(resp => {
          expect(resp.body.data).to.have.property("upvotes", 1)
        })
    })
    after(() => {
      return request(server)
        .get(`/diaries/${validID}`)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .then(resp => {
          expect(resp.body[0]).to.have.property("upvotes", 1)
        })
    })
  })
  describe("when the id is invalid", () => {
    it("should return a 404 and a message for invalid diary id", () => {
      return request(server)
        .put("/diaries/1100001/vote")
        .expect(200)
    })
  })



  //Gets diary by id and deletes it
  describe("DELETE /diaries/:id", () => {
    describe("when the id is valid", () => {
      it("should get diary with the valid id and delete it", done => {
        request(server)
          .delete(`/diaries/${validID}`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err) => {
            done(err)
          })
      })
    })
    //Returns when diary id is invalid
    describe("when the id is invalid", () => {
      it("should return a 404 and a message for invalid diary id", () => {
        request(server)
          .delete("/diaries/0000")
          .expect(404)
          .expect({
            message: "Invalid diary ID!"
          })
      })
    })
  })

})