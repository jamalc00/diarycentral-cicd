let mongoose = require('mongoose');

let DiarySchema = new mongoose.Schema({
        type: String,
        genre: String,
        favorite: String,
        stars: {type: Number, default: 0},
        comment: String,
        upvotes: {type: Number, default: 0}
    },
    { collection: 'diariesdb' });

module.exports = mongoose.model('Diary', DiarySchema);
