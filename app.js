/*eslint no-unused-vars: "off" */
let express = require("express")
let path = require("path")
let favicon = require("serve-favicon")
let logger = require("morgan")
let cookieParser = require("cookie-parser")
let bodyParser = require("body-parser")

let routes = require("./routes/index")
let users = require("./routes/users")
let diaries = require("./routes/diaries.js")

let app = express()

// view engine setup
app.set("views", path.join(__dirname, "views"))
app.set("view engine", "ejs")

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, "public")))
if (process.env.NODE_ENV !== "test") {  
  app.use(logger("dev"))
}
app.use("/", routes)
app.use("/users", users)

//Our Custom Routes
app.get("/diaries", diaries.findAll)
app.get("/diaries/votes", diaries.findTotalVotes)
app.get("/diaries/all", diaries.findTotalDiaries)
app.get("/diaries/:id", diaries.findOne)
app.get("/diaries/type/:type", diaries.findType)
app.get("/diaries/genre/:genre", diaries.findGenre)
app.post("/diaries/search", diaries.findFuzzy)
app.post("/diaries",diaries.addDiary)
app.put("/diaries/:id/vote", diaries.incrementUpvotes)
app.put("/diaries/:id/update", diaries.updateDiary) //Updates diary
app.delete("/diaries/:id", diaries.deleteDiary)
app.delete("/diaries/genre/:genre", diaries.deleteGenre)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error("Not Found")
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get("env") === "development") {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500)
    res.render("error", {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500)
  res.render("error", {
    message: err.message,
    error: {}
  })
})

module.exports = app
